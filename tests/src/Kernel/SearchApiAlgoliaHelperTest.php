<?php

namespace Drupal\Tests\search_api_algolia\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests helper.
 *
 * @group search_api_algolia
 */
class SearchApiAlgoliaHelperTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['search_api_algolia', 'search_api_algolia_test'];

  /**
   * Search API Algolia Helper service.
   *
   * @var \Drupal\search_api_algolia\SearchApiAlgoliaHelper
   */
  protected $helper;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->helper = $this->container->get('search_api_algolia.helper');
  }

  /**
   * Tests client creation and client config alter.
   */
  public function testBuildAlgoliaSearchClient() {
    $expected_app_id = 'test_app_id';
    $expected_api_key = 'test_api_key';

    $expected_connect_timeout = 15;
    $expected_default_headers = [
      'X-Algolia-Search-Client-Custom-Header' => 'test value',
    ];

    $client = $this->helper->buildAlgoliaSearchClient($expected_app_id, $expected_api_key);
    /** @var \Algolia\AlgoliaSearch\Config\SearchConfig $config */
    $config = $this->getProtectedProperty($client, 'config');

    static::assertEquals($expected_app_id, $config->getAppId());
    static::assertEquals($expected_api_key, $config->getApiKey());
    static::assertEquals($expected_connect_timeout, $config->getConnectTimeout());
    static::assertEquals($expected_default_headers, $config->getDefaultHeaders());
  }

  /**
   * Allows get access to protected properties.
   *
   * @param mixed $obj
   *   Object or instance.
   * @param mixed $prop
   *   Property name.
   *
   * @return mixed
   *   The value.
   *
   * @throws \ReflectionException
   */
  protected function getProtectedProperty($obj, $prop) {
    $reflection = new \ReflectionClass($obj);
    $property = $reflection->getProperty($prop);
    $property->setAccessible(TRUE);
    return $property->getValue($obj);
  }

}
