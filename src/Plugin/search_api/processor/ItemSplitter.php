<?php

namespace Drupal\search_api_algolia\Plugin\search_api\processor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\search_api\Processor\FieldsProcessorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Splits a single item/object/record into multiple items/objects/records.
 *
 * This may help avoid hitting the max size for a single record in Algolia.
 * More information: https://support.algolia.com/hc/en-us/articles/4406981897617-Is-there-a-size-limit-for-my-index-records-
 *
 * @SearchApiProcessor(
 *   id = "algolia_item_splitter",
 *   label = @Translation("Algolia item splitter"),
 *   description = @Translation("Splits items into multiple Algolia records to avoid hitting the record size limit (<a href='https://support.algolia.com/hc/en-us/articles/4406981897617-Is-there-a-size-limit-for-my-index-records-'>More information</a>)."),
 * )
 */
class ItemSplitter extends FieldsProcessorPluginBase {

  /**
   * The logger channel.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.channel.search_api_algolia');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'record_size_limit' => 10000,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['messages'] = [
      '#theme' => 'status_messages',
      '#message_list' => [
        'warning' => [
          $this->t('This processor requires that you set the <b>distinct</b> and <b>attributeForDistinct</b> settings in Algolia configuration. For more information, refer to the <a href="@docs_url" target="_blank">documentation</a>.', [
            '@docs_url' => 'https://www.algolia.com/doc/guides/sending-and-managing-data/prepare-your-data/how-to/indexing-long-documents/#how-to-enable-the-distinct-feature',
          ]),
          $this->t('This processor requires that you add <b>parent_record</b> to the filter-only attributes for faceting in Algolia configuration. For more information, refer to the <a href="@docs_url" target="_blank">documentation</a>.', [
            '@docs_url' => 'https://www.algolia.com/doc/guides/managing-results/refine-results/faceting/how-to/declaring-attributes-for-faceting-with-dashboard',
          ]),
        ],
      ],
      '#status_headings' => ['warning' => t('Warning message')],
    ];

    $form['record_size_limit'] = [
      '#type' => 'number',
      '#field_suffix' => $this->t('bytes'),
      '#title' => $this->t('Record size limit'),
      '#description' => $this->t('Maximum number of bytes a record can contain before its fields are split into parts. More information about limits can be found in the <a href="@documentationUrl">Algolia documentation</a>.', [
        '@documentationUrl' => 'https://support.algolia.com/hc/en-us/articles/4406981897617-Is-there-a-size-limit-for-my-index-records',
      ]),
      '#default_value' => $this->configuration['record_size_limit'],
    ];

    return $form;
  }

  /**
   * Split an item into multiple items.
   */
  public function splitItem(array $item): array {
    $recordSizeLimit = $this->configuration['record_size_limit'];
    $objectId = $item['objectID'];

    // Create a base item with all common, non-split field values.
    $baseItem = array_diff_key($item, array_flip($this->configuration['fields']));

    // Skip items whose base item is already too big.
    // Measure lengths using JSON_UNESCAPED_UNICODE to get the actual UTF-8
    // byte count.
    $baseItemLength = strlen(json_encode($baseItem, JSON_UNESCAPED_UNICODE));
    if ($baseItemLength > $recordSizeLimit) {
      $this->logger->error("Cannot split item since its common fields are bigger than the record size limit. You'll need to enable splitting for more fields. Item ID: @item_id", [
        '@item_id' => $objectId,
      ]);
      return [$item];
    }

    // Skip items that are already small enough.
    $fullItemLength = strlen(json_encode($item, JSON_UNESCAPED_UNICODE));
    if ($fullItemLength <= $recordSizeLimit) {
      return [$item];
    }

    // Split the item into multiple items.
    $splitIndex = 0;
    $splitFields = array_intersect_key($item, array_flip($this->configuration['fields']));
    $splits = [$splitIndex => $baseItem + ['parent_record' => 'self']];

    while ($splitFields !== []) {
      $fieldName = key($splitFields);
      $fieldValues = array_shift($splitFields);
      // Ensure we have an array to loop over.
      $fieldValues = is_array($fieldValues) ? $fieldValues : [$fieldValues];

      while ($fieldValue = array_shift($fieldValues)) {
        $itemSplitWithFieldValue = $splits[$splitIndex];
        $itemSplitWithFieldValue[$fieldName][] = $fieldValue;
        $itemSplitWithFieldValueLength = strlen(json_encode($itemSplitWithFieldValue, JSON_UNESCAPED_UNICODE));
        $fieldValueGoesOverLimit = $itemSplitWithFieldValueLength > $recordSizeLimit;

        // Keep adding field values to the current split
        // until it goes over the limit.
        if (!$fieldValueGoesOverLimit) {
          $splits[$splitIndex] = $itemSplitWithFieldValue;
          continue;
        }

        // Start a new split.
        $splitIndex++;
        $splits[$splitIndex] = $baseItem;
        $splits[$splitIndex]['objectID'] = $this->getSplitObjectId($objectId, $splitIndex);
        $splits[$splitIndex]['parent_record'] = str_replace(':', '-', $objectId);

        // If the field value is bigger than the record size limit, split it.
        $itemSplitWithFieldValue = $splits[$splitIndex];
        $itemSplitWithFieldValue[$fieldName][] = $fieldValue;
        $itemSplitWithFieldValueLength = strlen(json_encode($itemSplitWithFieldValue, JSON_UNESCAPED_UNICODE));

        if ($itemSplitWithFieldValueLength > $recordSizeLimit) {
          // Calculate available space in bytes.
          $available = $recordSizeLimit - ($itemSplitWithFieldValueLength - strlen($fieldValue));
          // Ensure we have a positive limit.
          $limit = ($available > 0) ? $available : 1;
          $fieldValues = [...$this->splitText($fieldValue, $limit), ...$fieldValues];
        }
        else {
          $fieldValues = [$fieldValue, ...$fieldValues];
        }
      }
    }

    return $splits;
  }

  /**
   * Get the object ID for a split.
   *
   * @param string $objectId
   *   The original object ID.
   * @param int $splitIndex
   *   The split index.
   *
   * @return string
   *   The split object ID.
   */
  protected function getSplitObjectId(string $objectId, int $splitIndex): string {
    return $objectId . '-split-' . $splitIndex;
  }

  /**
   * Split a string into multiple parts without breaking multibyte characters.
   *
   * This method ensures the split happens within the given byte limit.
   *
   * @param string $value
   *   The string to split.
   * @param int $limit
   *   The maximum number of bytes for each part.
   *
   * @return string[]
   *   An array of parts.
   */
  protected function splitText(string $value, int $limit): array {
    // Ensure the value is UTF-8 encoded.
    $value = mb_convert_encoding($value, 'UTF-8', 'auto');
    $splits = [];
    $prefix = '';

    while (TRUE) {
      $value = ltrim($value);
      if (strlen($value) <= $limit) {
        $splits[] = $prefix . $value;
        break;
      }
      // mb_strcut cuts by bytes and is multibyte safe.
      $cutSubstring = mb_strcut($value, 0, $limit, 'UTF-8');
      // Try to find the last space in the cut portion.
      $pos = mb_strrpos($cutSubstring, ' ', 0, 'UTF-8');
      if ($pos === FALSE) {
        // If no space is found, use the entire cut substring.
        $pos = mb_strlen($cutSubstring, 'UTF-8');
      }
      // Get the part to split out.
      $splitPart = mb_substr($value, 0, $pos, 'UTF-8');
      $splits[] = $prefix . $splitPart;
      // Remove the split part from the value.
      $value = mb_substr($value, $pos, NULL, 'UTF-8');
      $prefix = '… ';
    }

    return $splits;
  }

}
